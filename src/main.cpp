// ====================================================================================
// ===    Helmet IR Transmitter
// ===    
// ===    De IR transmitter genereert 16 unieke infrarood codes
// ===    Deze codes worden door de IR led's in de schatkamervitrine uitgezonden en 
// ===    door de helmen opgevangen waneer deze er op gericht worden.
// ===    In tegenstelling tot de meeste IR codes wordt hier niet met lange en korte pulsen gewerkt. 
// ===    De pulsen zijn voor alle codes even lang. De interval tussen de pulsen is bij dit systeem van belang.
// ===    Meer informatie over deze IR codering is te lezen in de technische beschrijving van de helmen.
// ===
// ===    Hardware:
// ===    De IR led's worden aangestuurd door twee TPIC6A595DW schuifregisters. (serieel in - parallel uit) 
// ===    Deze schuifregisters zijn middels een SPI bus op een Arduino Nano aangesloten.
// ===    De schuifregisters zijn uitgerust met een tri-state uitgang. Deze voorziening wordt gebruikt om 
// ===    de 38Khz modulatie van het IR licht te realiseren.

// =====================================================================================




#include <Arduino.h>
#include <TimerOne.h>
#include <SPI.h>
#include "SWTimer.h"


#define MaxChan 16
#define NrTimers MaxChan

#define Toermalijn 1
#define Prehniet 2
#define Bariet 3
#define Fluoriet 4
#define Zwavel 5
#define Goud 6
#define Willemiet 7
#define Apatiet 8
#define Sodaliet 9
#define Bergkristal 10
#define Pyriet 11
#define Aragoniet 12
#define Calciet 13
#define Okeniet 14
#define Idle 15

SWTimer MTimer[NrTimers]; //array van softwaretimers 
int DelArr[MaxChan]; // Int array. Hierin wordt de pauze tussen de IR bursts bijgehouden
int Puls38K = 7; // Pin Puls38K levert het 38Khz signaal voor de IR signaalmodulatie
int RCK = 8; // Pin RCK stuurt de output sectie van het shiftregister.

// PatchTab is de patchtabel. Hiermee kunnen de diverse IR codes naar een uitgang naar keuze worden gepatched
int PatchTab[MaxChan]  = {Okeniet,Prehniet,Toermalijn,Bariet,
                          Fluoriet,Calciet,Zwavel,Goud, 
                          Sodaliet,Apatiet,Willemiet,Idle,
                          Aragoniet,Bergkristal,Pyriet,Idle};

#define BurstPauze 400

void T2Intr(void) // Dit is de timer2 interruptroutine. Wordt 2000 keer per seconde uitgevoerd!
{
int i;
  for(i=0;i<NrTimers;i++)
    MTimer[i].Update(); // Update alle gebruikte sw timers.
}

uint16_t IRFreq = 38000; // IR modulatie frequentie is 38000 Herz

void setup() 
{
int i;
  pinMode(RCK,OUTPUT); 
  digitalWrite(RCK,HIGH);
  Serial.begin(115200);

  tone(Puls38K,IRFreq); // Na het uitvoeren van dit commando verschijnt op uitgang Puls38K het 38Khz modulatiesignaal.
  SPI.begin(); // initialiseer SPI bus. Via deze bus worden de schuifregisters aangestuurd.
  Serial.print("spi begin");
  SPI.setDataMode(SPI_MODE0); 
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV128);

  Timer1.initialize(500);// = 0.0005
  Timer1.attachInterrupt(T2Intr);
  
  for(i=0; i<MaxChan; i++)
  {
    MTimer[i].Set(500);  // Initialiseer timer en delay array
    DelArr[i] = 0;
  }  
}

void DataOut(uint16_t dat) // Met deze functie wordt de TPIC6A595DW (serieel - parallel schuifregister) aangestuurd
{
  SPI.transfer(highByte(dat));
  SPI.transfer(lowByte(dat));
  digitalWrite(RCK,LOW);
  digitalWrite(RCK,HIGH); 
}  

void Do_Timers(void)  // Deze functie checkt of de swtimers expired zijn. Zo ja dan worden deze opnieuw gestart en wordt ook een IR burst gestart.
                      // Als er echter al 10 bursts gegeven zijn dan wordt de timer met een langere wachttijd geladen. Hierdoor ontstaan pauzes 
                      // tussen de bursts.
{
uint16_t Ch_Out; 

int i;

  Ch_Out = 0;
  for(i=0; i<MaxChan; i++)
  {
    if( MTimer[i].Expired())  // Timer Expired?
    {
      DelArr[i]++;
      if (DelArr[i] > 10) // Als er 10 IR bursts zijn geweest volgt een pauze
      {
        MTimer[i].Set(BurstPauze); // 
        DelArr[i] = 0;
      }  
      else
      {
        MTimer[i].Set(PatchTab[i]+4); // Anders (geen pauze) wordt de swtimer opnieuw geladen
        Ch_Out|= (1<<i); // en volgt een nieuwe burst (Zet bit in Ch_Out word)
      }
    }
  }
  if(Ch_Out !=0)
  {
    DataOut(Ch_Out); // De waarde van Ch_Out wordt via SPI naar de schuifregisters gestuurd. Elk bitje dat = 1 veroorzaakt een burst op het bijhorend kanaal 
    delayMicroseconds(100); // burst lengte
    DataOut(0x0000); // Burst einde op alle kanalen
  }
}


void loop()
{
  Do_Timers(); 
  delayMicroseconds(5); 
}
