# Helmet IR Transmitter

De schattenjacht is een spel dat in de schatkamer van de Aarde zaal wordt
gespeeld. Met speciaal geprepareerde helmen wordt de bezoeker uitgedaagd om 14
“schatten” te zoeken.

In de vitrine in schatkamer van de Aardezaal  zijn 14 objecten voorzien van een
ir-led die een code uitzendt.  De schattenjacht-helmen kunnen door middel van
de Infrarood-signalen deze objecten vinden

De 14 infrarood codes worden gegenereerd door de IR transmitter. De software
van dit apparaat wordt hier beschreven.

De software loopt op een Arduino Nano. Deze is uitgerust met een Atmel
ATmega328  processor. Naar huidige (2019) maatstaven is dit een bescheiden
systeem maar het is krachtig genoeg om de software van de IR transmitter uit te
voeren.

## Hardware

De Arduino is op een printplaat gemonteerd. Op deze print bevindt zich ook de 
hardware om de 14 IR leds aan te sturen. 

De IR led's worden aangestuurd door twee TPIC6A595DW schuifregisters. (serieel in - parallel uit) 
Deze schuifregisters zijn middels een SPI bus op een Arduino Nano aangesloten.
De schuifregisters zijn uitgerust met een tri-state uitgang. Deze voorziening 
wordt gebruikt om de 38Khz modulatie van het IR licht te realiseren.

![Schuifregisters/leddriver](pics/leddriver-chip.png)

De bits die via de seriële ingang (SER IN)  binnenkomen sturen de acht
uitgangslijnen (drains) aan. Voor deze toepassing zijn de twee chips in cascade
geschakeld. Hierbij wordt de SER OUT uitgang van de eerste chip aan de SER IN
van de tweede chip verbonden. Op deze wijze ontstaat een 16 bits
schuifregister.

## Modulatie van het ir signaal

Voor een goede werking van infrarood-signaaloverdracht is het nodig het
ir-licht te moduleren met een frequentie van 38kHz. De Arduino heeft een
functie voor het opwekken van frequenties.  het commando 
`tone(pin, frequency);` 
zorgt ervoor dat op de uitgang een blokgolf met de gewenste
frequentie kan worden afgenomen. Dit signaal wordt op het schuifregister op pin
/G aangesloten. De modulatie van het uitgangssignaal wordt hierdoor tot stand
gebracht.

## De infraroodcode

De infraroodcode die bij het schattenjacht systeem gebruikt wordt is erg
simpel. Alle IR pulsen zijn even lang. Het verschil in interval tussen de
pulsen is van belang. Dit maakt het genereren van de codes relatief simpel.  De
minimale interval tussen twee pulsen is 2 millisec. De intervallen verschillen
minimaal 0,5 mS van elkaar. De intervallen van de diverse codes kan berekend
worden met deze formule: `I = 2 + n x 0.5`. Waarbij I = interval in millisec en n
het nummer van de code. 

Voorbeeld: De interval voor code 6 is 2 + (6 x 0.5) = 5

Tabel van objecten in de vitrine met bijhorende intervallen

| Nummer | Object | Interval tussen pulsen in ms |
|--------|--------|------------------------------|
| 1 | Toermalijn | 2,5 |
| 2 | Prehniet | 3|
| 3 | Bariet | 3,5|
| 4 | Fluoriet | 4|
| 5 | Zwavel | 4,5|
| 6 | Goud | 5|
| 7 | Willemiet | 5,5|
| 8 | Apatiet | 6|
| 9 | Sodaliet | 6,5|
| 10 | Bergkristal | 7|
| 11 | Pyriet | 7,5|
| 12 | Aragoniet | 8|
| 13 | Calciet | 8,5|
| 14 | Okeniet | 9|


## SWTimer library

De software maakt gebruik van een timer library die het mogelijk maakt om
software timers te definiëren.

Met gebruikmaking van de SWtimer library is een scheduler gemaakt die de 14
verschillende IR codes opwekt. Per kanaal worden steeds 10 IR pulsen
gegenereerd die worden gescheiden door de bij dat kanaal horende interval.
Daarna volgt een pauze van 200ms.

De SWTimer library definieert  SWTimer objecten. Deze objecten kunnen gebruikt worden om tijdsintervallen te genereren. 

Een SWTimer object bestaat uit variabelen:
 * uint32_t  _TimerVar
 * boolean _TimerFlag

En uit functies: 

 * `Set()`. Met deze functie wordt _TimerVar van een waarde voorzien.
 * `uint32_t Get()`. Met deze functie kan de waarde van _TimerVar gelezen worden
 * `Halt()`. Deze functie stopt de SWTimer. 
 * `boolean Expired()`. Met deze functie wordt de waarde van _TimerFlag gelezen.
 * `Update()`. Deze functie verlaagt de waarde van _TimerVar.  _TimerFlag wordt true als TimerVar = 0.

Een SWTimer object wordt geïnstantieerd door deze als variabele te declareren.
De timer wordt gestart door de counter een waarde te geven met de Set() functie.
Met de Expired() functie kan getest worden of de timer klaar is. (afgeteld naar 0)
Als men de timer inactief wil maken kan dit met de functie Halt().

Het is de bedoeling dat de Update() functie regelmatig wordt uitgevoerd. Het beste kan deze functie vanuit een hardware-timer ISR worden aangeroepen. De frequentie van aanroepen bepaalt de resolutie van de timer.

## Patch tabel

Er is een patch-tabel gemaakt waarmee elke IR code naar elke gewenste uitgang
kan worden gestuurd.
