

#include "Timer5.h"
#include "SWTimer.h"

#define NrTimers 10

int Teller = 0;

SWTimer MTimer[NrTimers];

void setup() 
{
  MyTimer5.begin(100);  // Timer interrupt every 10 Ms (100Hz) 
  // define the interrupt callback function
  MyTimer5.attachInterrupt(Timer5_IRQ);
  Serial.begin(19200);
  MTimer[3].Set(500);
}


// =================================================================================================
// Timer 5   Main system timer
// =================================================================================================

void Timer5_IRQ(void) 
{
    static bool on = false;
 int i;
 Teller++;
 //--------------- Software Timers:    
   for(i=0;i<NrTimers;i++)
      MTimer[i].Update();
}


void loop()
{
  // put your main code here, to run repeatedly:
  Serial.println(MTimer[3].Get());
  if( MTimer[3].Expired())
  {
    MTimer[3].Set(500);
    Serial.println("MTimer!!!!!");
    
  }  
  delay(1000);
}
